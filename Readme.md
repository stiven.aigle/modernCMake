# Modern CMake
## Compilation
Avec CMake < 3.21 
```cmake
cmake -S . - B build
cmkae --build build
```
Avec CMake >=3.21
```cmake
cmake --preset default
cmake --build --preset default
```

## Ressources
- [Documentation](https://cmake.org/cmake/help/latest/index.html)
- [It's Time To Do CMake Right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
- [Deep CMake for Library Author](https://www.youtube.com/watch?v=m0DwB4OvDXk)
- [More Modern CMake](https://www.youtube.com/watch?v=y7ndUhdQuU8)
- [Oh No! More Modern CMake](https://www.youtube.com/watch?v=y9kSr5enrSk)
